'use strict';

// GJS's Built-in modules
const Lang = imports.lang;
const Mainloop = imports.mainloop;

// Gnome APIs
const Clutter = imports.gi.Clutter;
const St = imports.gi.St;

// Gnome Shell
const Main = imports.ui.main;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Tweener = imports.ui.tweener;

var PomodoroMenu = class PomodoroMenu extends PanelMenu.Button {
    _init() {
        super._init(0.5, `${Me.metadata.name}`, false);

        this._duration = 0;
        this._durationTemp = 0;
        this._toggleTimer = false;
        this._toggleSwitch = null;
        this._timerApp = new St.Label({
            text: `00:00:00`,
            style_class: 'panel-button__label',
            y_expand: true,
            y_align: Clutter.ActorAlign.CENTER
        });

        let boxApp = new St.BoxLayout({
            style_class: 'panel-button',
            x_align: St.Align.MIDDLE
        });
        let iconApp = new St.Icon({
            icon_name: 'document-open-recent-symbolic',
            style_class: 'system-status-icon'
        });
        let dropdownIcon = PopupMenu.arrowIcon(St.Side.BOTTOM);
        boxApp.add(iconApp);
        boxApp.add(this._timerApp);
        boxApp.add(dropdownIcon);
        this.actor.add_child(boxApp);

        this.menu.box.style_class = 'panel-button__popup-menu';

        let timerBox = new St.BoxLayout({
            style_class: 'panel-button__input-container'
        });
        let hourEntry = new St.Entry({
            hint_text: `Heures`,
            style_class: 'panel-button__hour'
        });
        let sepHour = new St.Label({
            text: `:`,
            style_class: 'panel-button__sep',
            x_expand: true,
            x_align: Clutter.ActorAlign.CENTER,
            y_expand: true,
            y_align: Clutter.ActorAlign.CENTER
        });
        let minEntry = new St.Entry({
            hint_text: `Minutes`,
            style_class: 'panel-button__min'
        });
        let sepMin = new St.Label({
            text: `:`,
            style_class: 'panel-button__sep',
            x_expand: true,
            x_align: Clutter.ActorAlign.CENTER,
            y_expand: true,
            y_align: Clutter.ActorAlign.CENTER
        });
        let secEntry = new St.Entry({
            hint_text: `Secondes`,
            style_class: 'panel-button__sec'
        });
        let valid = new PopupMenu.PopupMenuItem(`Appliquer`);
        this._toggleSwitch = new PopupMenu.PopupSwitchMenuItem(`Démarrer`, false);
        timerBox.add(hourEntry);
        timerBox.add(sepHour);
        timerBox.add(minEntry);
        timerBox.add(sepMin);
        timerBox.add(secEntry);
        this.menu.box.add(timerBox);
        this.menu.addMenuItem(valid);
        this.menu.addMenuItem(this._toggleSwitch);

        valid.connect('activate', Lang.bind(this, function() {
            // converting string to int
            let h = hourEntry.get_text() * 1;
            let m = minEntry.get_text() * 1;
            let s = secEntry.get_text() * 1;
            this.testEntry(h, m, s);
        }));

        this._toggleSwitch.connect('toggled', Lang.bind(this, function(object, value) {
            if (value) {
                this.startTimer();
            } else {
                this.stopTimer();
            }
        }));
    }

    testEntry(h, m, s) {
        let msgError = {
            supZero: `Veuillez inscrire une durée supérieure à 0.`,
            notNumber: `Veuillez inscrire uniquement des chiffres.`
        };

        // converting input to seconds
        this._duration = (h * 60 * 60) + (m * 60) + s;
        if (this._duration < 1) {
            Main.notify(`${Me.metadata.name}`, msgError.supZero);
        } else if (isNaN(this._duration)) {
            Main.notify(`${Me.metadata.name}`, msgError.notNumber);
        } else {
            this._durationTemp = this._duration;
            this.setDuration();
        }
    }

    pad(number) {
        number = number.toString();
        if (number.length < 2) { number = "0" + number; }
        return number;
    }

    setDuration() {
        let sec = this._durationTemp % 60;
        let min = Math.floor(this._durationTemp / 60) % 60;
        let hour = Math.floor((this._durationTemp / 60) / 60);

        // TODO: delete `+` to have one string without concatenation
        this._timerApp.set_text(`${this.pad(hour)}` + `:` + `${this.pad(min)}` + `:` + `${this.pad(sec)}`);
    }

    countdown() {
        // Using Mainloop.timeout_add() but not setTimeout()
        timerId = Mainloop.timeout_add(1000, () => {
            if (this._durationTemp > 0) {
                this._durationTemp--;
                this.setDuration();
                this.countdown();
            } else {
                this.endTimer();
            }
        });
    }

    startTimer() {
        if (!this._toggleTimer) {
            this._toggleTimer = true;
            this.countdown();
            Main.notify(`${Me.metadata.name}`, `Démarré`);
            this._toggleSwitch.label.set_text(`Arrêter`);
        }
    }

    stopTimer() {
        // Using Mainloop.source_remove() but not clearTimeout()
        Mainloop.source_remove(timerId);
        this._toggleTimer = false;
        Main.notify(`${Me.metadata.name}`, `Arrêté`);
        this._toggleSwitch._switch.setToggleState(false);
        this._toggleSwitch.label.set_text(`Démarrer`);
    }

    endTimer() {
        // Using Mainloop.source_remove() but not clearTimeout()
        Mainloop.source_remove(timerId);
        this._toggleTimer = false;
        Main.notify(`${Me.metadata.name}`, `Terminé`);
        this._toggleSwitch._switch.setToggleState(false);
        this._toggleSwitch.label.set_text(`Démarrer`);
    }

    destroy() {
        super.destroy();
    }
};

let app, timerId;

function init() {
    log(`Initializing ${Me.metadata.name} version ${Me.metadata.version}`);
}

function enable() {
    log(`Enabling ${Me.metadata.name} version ${Me.metadata.version}`);
    app = new PomodoroMenu();
    Main.panel.addToStatusArea(`${Me.metadata.name}`, app, 0, 'center');
}

function disable() {
    log(`Disabling ${Me.metadata.name} version ${Me.metadata.version}`);
    if (app !== null) {
        Mainloop.source_remove(timerId);
        app.destroy();
        app = null;
    }
}

